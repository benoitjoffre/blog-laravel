<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Crud;
use\App\Article;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
        $user = Auth::user();
        $articles = Article::where('author_id', $user->id)->get();
        return view('crud.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'extract'=> 'required',
            'content' => 'required',
            'image' => 'required'
          ]);
          $crud = new Article([
            'title' => $request->get('title'),
            'extract'=> $request->get('extract'),
            'content' => $request->get('content'),
            'image'=> $request->get('image'),
            'author_id' => auth()->id()
          ]);
          $crud->save();
          return redirect('/crud')->with('success', 'Stock has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('crud.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'extract'=> 'required',
            'content' => 'required',
            'image' => 'required'
          ]);

          $crud = Article::find($id);
            $crud->title = $request->get('title');
            $crud->extract = $request->get('extract');
            $crud->content = $request->get('content');
            $crud->image = $request->get('image');
            $crud->author_id = auth()->id();

            $crud->save();
            return redirect('/crud')->with('success', 'Stock has been updated successfully');
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = Article::find($id);
     $crud->delete();

     return redirect('/crud')->with('success', 'Stock has been deleted successfully');
          
    }
}