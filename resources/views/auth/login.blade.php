@extends('layouts.app')

@section('content')
<div class="container">
        <div class="bg-card-create">
              <form class="form-horizontal" method="post" action="{{ route('login') }}">
                @csrf
                <h3 class="text-center">LOGIN</h3>
                <div class="form-group">
                        <label for="username" class="col-md-2 control-label">{{ __('Username') }}</label>

                        <div class="col-md-10">
                            <input id="username" placeholder="Type your username here..." type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                            @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
        
                    <div class="form-group">
                            <label for="password" class="col-md-2 control-label">{{ __('Password') }}</label>

                            <div class="col-md-10">
                                <input id="password" placeholder="Type your passwords here..." type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
        
                  
        
                    
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                          <button type="submit" class="btn btn-primary">LOGIN</button>
                        </div>
                      </div>
              </form>
          
          </div>
        
        </div>
        </div>

        
{{-- <div class="container">
        <div class="col-md-8">
            <div class="bg-card-create">
                

                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        @csrf
<H3 class="text-center">LOGIN</H3>
                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username" placeholder="Type your username here..." type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" placeholder="Type your passwords here..." type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                    <div class="form-group">
                                            <div class="col-md-10 col-md-offset-2">
                                              <button type="submit" class="btn btn-primary">LOGIN</button>
                                            </div>
                                          </div>
                                          <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="form-check-label" for="remember">
                                                        {{ __('Remember Me') }}
                                                    </label>
                                            </div>
                                          
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   
</div> --}}
@endsection
