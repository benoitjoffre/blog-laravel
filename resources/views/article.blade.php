<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>
<body>

    <section class="container">
        <div class="card">
                <div class="card-body">
                    <div class="title-return-login">
                            <a href="/"><input type="button" class="btn-bluedark" value="RETURN"></a>
                            <h3 class="card-title text-center">{{$article->title}}</h3> 
                            @guest
                            @if (Route::has('register'))
                            <a href="{{ route('login') }}"><input type="button" class="btn-bluesky" value="LOGIN"></a>  
                            @endif
                          @else
                            <a href="{{ url('home') }}"><input type="button" class="btn-bluesky" value="PROFIL"></a> 
                    @endguest
                        </div>
                        <img class="card-img-top" src="{{$article->image}}" alt="">
                        <hr class="my-4">
                        <p class="card-text text-right">Published : {{$article->created_at}}</p>
                        <hr class="my-4">
                        <p class="card-text">{{$article->content}}</p>
                        <footer class="blockquote-footer text-right">
                            <cite>{{$article->author->username}}</cite>
                        </footer>
                </div>
            </div>    
    </section>
 
</body>
</html>