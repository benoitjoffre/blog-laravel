@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
    <section class="container">
        <div class="card-columns">
                @foreach ( $articles as $article )
                <a href="{{route('article',['id' => $article->id])}}">
                    <div class="card">
                        <div class="card-body">
        
                                <h3 class="card-title text-center">{{$article->title}}</h3>
                                <img class="card-img-top" src="{{$article->image}}" alt="">
                                <p class="card-text text-justify">{{$article->extract}}</p>
                                <hr class="my-4">
                                <p class="card-text text-left">Published : {{$article->created_at}}</p>
                                <footer class="blockquote-footer">
                                    <cite>{{$article->author->username}}</cite>
                                </footer>
                        </div>
                    </div>    
                </a> 
                @endforeach       
        
        </div>
    </section>
@endsection