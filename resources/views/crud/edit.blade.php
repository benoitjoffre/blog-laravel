@extends('layouts.app')

@section('content')
<div class="container">
<div class="bg-card-create">
 
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form class="form-horizontal" method="post" action="{{ route('crud.update', $article->id) }}">
        @method('PATCH')
        @csrf
        <h3 class="text-center">EDIT</h3>
        <div class="form-group">
            <label for="title" class="col-md-2 control-label">Title</label>

            <div class="col-md-10">
            <input type="text" class="form-control" name="title" placeholder="Title" value="{{$article->title}}">
            </div>
          </div>

          <div class="form-group">
            <label for="extract" class="col-md-2 control-label">Extract</label>

            <div class="col-md-10">
              <textarea type="text" class="form-control" name="extract" placeholder="Extract">{{$article->extract}}</textarea>
            </div>
          </div>

          <div class="form-group">
              <label for="content" class="col-md-2 control-label">Content</label>

              <div class="col-md-10">
                <textarea type="text" class="form-control" name="content" placeholder="Content">{{$article->content}}</textarea>
              </div>
            </div>

            <div class="form-group">
              <label for="image" class="col-md-2 control-label">Image</label>

              <div class="col-md-10">
                <input type="text" class="form-control" name="image" placeholder="Image" value="{{$article->image}}">
              </div>
            </div>
            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                  <button type="submit" class="btn btn-primary">UPDATE</button>
                </div>
              </div>
      </form>
  
  </div>

</div>
</div>
@endsection