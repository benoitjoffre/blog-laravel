@extends('layouts.app')

@section('content')
<section class="container">
    <div class="table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl">
    <table class="table table-striped table-hover">
        <thead>
            <tr>
              <td>ID</td>
              <td>Title</td>
              <td>Extract</td>
              <td>Date</td>
              <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($articles as $article)
            <tr>
                <td>{{$article->id}}</td>
                <td>{{$article->title}}</td>
                <td>{{$article->extract}}</td>
                <td>{{$article->created_at}}</td>
                <td class="info-posts"><a href="{{ route('crud.edit',$article->id)}}" class="btn-bluesky pencil"><i class="fas fa-pencil-alt"></i></a></td>
                <td class="info-posts">
                    <form action="{{ route('crud.destroy', $article->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn-danger1" type="submit"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>    
    </div>
</section>
  @endsection