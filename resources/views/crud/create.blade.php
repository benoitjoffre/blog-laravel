@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="bg-card-create">

		
		@if ($errors->any())
			@foreach ($errors->all() as $error)
				<div class="alert alert-dismissible alert-danger">
				  <button type="button" class="close" data-dismiss="alert">×</button>
				  <strong>Oh snap!</strong>{{ $error }}
				</div>
			@endforeach
		@endif

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title text-center">Add Article</h3>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" action="{{ route('crud.store') }}" method="POST">
					{{ csrf_field() }}
			  <fieldset>

			  	<div class="form-group">
			      <label for="title" class="col-md-2 control-label">Title</label>

			      <div class="col-md-10">
			        <input type="text" id="title" class="form-control" name="title" placeholder="Title">
			      </div>
			    </div>

			    <div class="form-group">
			      <label for="extract" class="col-md-2 control-label">Extract</label>

			      <div class="col-md-10">
			        <textarea id="extract" type="text" class="form-control" name="extract" placeholder="Extract"></textarea>
			      </div>
			    </div>

			    <div class="form-group">
                    <label for="content" class="col-md-2 control-label">Content</label>
  
                    <div class="col-md-10">
                      <textarea type="text" id="content" class="form-control" name="content" placeholder="Content"></textarea>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="image" class="col-md-2 control-label">Image</label>
  
                    <div class="col-md-10">
                      <input type="text" id="image" class="form-control" name="image" placeholder="Image">
                    </div>
                  </div>
                  
			    <div class="form-group">
			      <div class="col-md-10 col-md-offset-2">
			        <button type="submit" class="btn btn-primary">SUBMIT</button>
			      </div>
			    </div>
			  </fieldset>
			</form>
		 </div>
		</div>
	</div>
	</div>
@endsection